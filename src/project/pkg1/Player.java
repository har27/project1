public abstract class Player {

    private String name; 

    /**
     * A constructor that allows you to set the player's unique ID
     *
     * @param name 
     */
    public Player(String name) {
        this.name = name;
    }

    
    public String getName() {
        return name;
    }

    /**
     * Ensure that the playerID is unique
     *
     * @param name the player name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    public abstract void play();

}