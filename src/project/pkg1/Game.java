import java.util.ArrayList;

public abstract class Game {

    private final String name;
    private ArrayList<Player> players;

    public Game(String name) {
        this.name = name;
        players = new ArrayList();
    }

   
    public String getName() {
        return name;
    }

    /**
     * @return the players of this game
     */
    public ArrayList<Player> getPlayers() {
        return players;
    }

    /**
     * @param players the players of this game
     */
    public void setPlayers(ArrayList<Player> players) {
        this.players = players;
    }

    
    public abstract void play();

    /**
     * this method is used to declare and display a winning player, when game is over, .
     */
    public abstract void declareWinner();

}
